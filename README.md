# ImagesPipeline

This repository contains several example of desktop and application Workspaces images.

# Install Docker & Docker Compose

    sudo apt-get update -y
    sudo apt-get upgrade -y

    sudo apt-get --no-install-recommends install -y \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg-agent \
      software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) \
      stable"
    sudo apt-get update
    sudo apt-get --no-install-recommends install -y docker-ce docker-ce-cli containerd.io
    sudo apt-get --no-install-recommends install -y python3-pip python3-setuptools
    sudo python3 -m pip install setuptools docker-compose

# Manual Deployment

To download the images:

    git clone https://gitlab.com/arbitrllc/imagespipeline.git
    
  Type Username & Password if prompted.
    
    cd imagespipeline/


To build the provided images:

    sudo docker build -t arbitrllc/firefox:dev -f dockerfile-kasm-firefox .


While these image are primarily built to run inside the Workspaces platform, they can also be executed manually. Please note that certain functionality, such as audio, uploads, downloads, and microphone pass-through are only available within the Kasm platform.

```
sudo docker run --rm  -it --shm-size=512m -p 6901:6901 -e VNC_PW=password arbitrllc/firefox:dev
```

The container is now accessible via a browser : `https://<IP>:6901`

 - **User** : `kasm_user`
 - **Password**: `password`

Any available image can be run by providing the respective dockerfile:

    sudo docker build -t arbitrllc/{{container_name}}:dev -f {{dockerfile_name}} .

    sudo docker run --rm  -it --shm-size=512m -p 6901:6901 -e VNC_PW=password arbitrllc/{{container_name}}:dev

  rename the `container_name` & `dockerfile_name` as required.

Following are the available image dockerfiles:

- dockerfile-kasm-brave
- dockerfile-kasm-centos-7-desktop
- dockerfile-kasm-chrome
- dockerfile-kasm-chrome-flash
- dockerfile-kasm-desktop
- dockerfile-kasm-desktop-deluxe
- dockerfile-kasm-discord
- dockerfile-kasm-doom
- dockerfile-kasm-edge
- dockerfile-kasm-firefox
- dockerfile-kasm-firefox-flash
- dockerfile-kasm-firefox-mobile
- dockerfile-kasm-gimp
- dockerfile-kasm-insomnia
- dockerfile-kasm-maltego
- dockerfile-kasm-only-office
- dockerfile-kasm-postman
- dockerfile-kasm-rdesktop
- dockerfile-kasm-remmina
- dockerfile-kasm-signal
- dockerfile-kasm-slack
- dockerfile-kasm-steam
- dockerfile-kasm-sublime-text
- dockerfile-kasm-teams
- dockerfile-kasm-terminal
- dockerfile-kasm-tor-browser
- dockerfile-kasm-ubuntu-bionic-desktop
- dockerfile-kasm-vlc
- dockerfile-kasm-vmware-horizon
- dockerfile-kasm-vs-code
- dockerfile-kasm-zoom
- dockerfile-kasm-zsnes
